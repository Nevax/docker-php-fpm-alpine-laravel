FROM php:7.4-fpm-alpine as php

WORKDIR /var/www/html

# Installing required dependencies
RUN apk --no-cache add $PHPIZE_DEPS imagemagick imagemagick-dev jpegoptim optipng gifsicle icu-dev bzip2-dev libzip-dev zip shadow freetype-dev libjpeg-turbo-dev postgresql-dev \
        # Install dependancies
        && docker-php-ext-configure gd > /dev/null \
        && docker-php-ext-configure zip > /dev/null \
        && docker-php-ext-install -j$(nproc) intl zip bz2 opcache exif bcmath pcntl gd pdo_pgsql > /dev/null \
        && pecl install -o -f redis > /dev/null \
        && pecl install -o -f imagick > /dev/null \
        && docker-php-ext-enable redis \
        && docker-php-ext-enable imagick \
        # Change group and user id
        && groupmod -g 1000 www-data \
        && usermod -u 1000 www-data \
        # Allow www-data group to read php config files
        && chown root:www-data -R /usr/local/etc/php \
        && chmod g+r -R /usr/local/etc/php \
        && chown -R www-data:www-data /var/www/html \
        # Cleanup
        && apk del $PHPIZE_DEPS shadow \
        && rm -rf /tmp/* /usr/local/lib/php/doc/* /var/cache/apk/* \
        && rm -rf /tmp/pear ~/.pearrc
